"""
Contains the original CertificateTemplate and EnrollmentService Classes from certipy < 2.0.

Notes: Certipy 2.0+ ripped all the exploit validation code out of their project, and in its place added Bloodhound
input generation. For simplicity, we didn't want to integrate yet another toolset, and I didn't want to write all the
exploit validation code myself, so this file originally was just a placeholder while I ripped old code from certipy (
commit #0180629d6f37de9c1c44f35c78bb7c8225b37827) """

from certipy.ldap import LDAPEntry
import struct
from certipy.constants import (
    ACTIVE_DIRECTORY_RIGHTS,
    CERTIFICATION_AUTHORITY_RIGHTS,
    EXTENDED_RIGHTS_NAME_MAP,
    MS_PKI_CERTIFICATE_NAME_FLAG,
    MS_PKI_ENROLLMENT_FLAG,
    MS_PKI_PRIVATE_KEY_FLAG,
    OID_TO_STR_MAP,
)
from asn1crypto import x509
from certipy.security import ActiveDirectorySecurity, is_low_priv_sid
from typing import List

# some string constants to describe the privesc capabilities
ESC1 = "ESC1"
ESC2 = "ESC2"
ESC3 = "ESC3"
ESC3_1 = "ESC3_1"
ESC4 = "ESC4"
ESC6 = "ESC6"

SAN_REQUIRES_DNS = 'SubjectAltRequireDns'


def filetime_to_span(filetime: bytes) -> int:
    (span,) = struct.unpack("<q", filetime)

    span *= -0.0000001

    return int(span)


def span_to_str(span: int) -> str:
    if (span % 31536000 == 0) and (span // 31536000) >= 1:
        if (span / 31536000) == 1:
            return "1 year"
        return "%i years" % (span // 31536000)
    elif (span % 2592000 == 0) and (span // 2592000) >= 1:
        if (span // 2592000) == 1:
            return "1 month"
        else:
            return "%i months" % (span // 2592000)
    elif (span % 604800 == 0) and (span // 604800) >= 1:
        if (span / 604800) == 1:
            return "1 week"
        else:
            return "%i weeks" % (span // 604800)

    elif (span % 86400 == 0) and (span // 86400) >= 1:
        if (span // 86400) == 1:
            return "1 day"
        else:
            return "%i days" % (span // 86400)
    elif (span % 3600 == 0) and (span / 3600) >= 1:
        if (span // 3600) == 1:
            return "1 hour"
        else:
            return "%i hours" % (span // 3600)
    else:
        return ""


def filetime_to_str(filetime: bytes) -> str:
    return span_to_str(filetime_to_span(filetime))


class EnrollmentService:
    ATTRIBUTES = [
        "cn",
        "dNSHostName",
        "cACertificateDN",
        "cACertificate",
        "certificateTemplates",
    ]

    def __init__(
            self,
            entry: LDAPEntry,
            instance: "Find",
            edit_flags: int = None,
            security_descriptor: ActiveDirectorySecurity = None,
            enrollment_restrictions: ActiveDirectorySecurity = None,
    ):
        self.entry = entry
        self.instance = instance
        self.edit_flags = edit_flags
        self.security_descriptor = security_descriptor
        self.enrollment_restrictions = enrollment_restrictions

        self.ca_name = entry.get("cn")
        self.dns_name = entry.get("dNSHostName")
        self.subject_name = entry.get("cACertificateDN")
        # print(entry.get_raw("cACertificate"))
        cert = entry.get_raw("cACertificate")
        if isinstance(cert, list):
            cert = cert[0]
        ca_certificate = x509.Certificate.load(cert)[
            "tbs_certificate"
        ]

        self.serial_number = hex(int(ca_certificate["serial_number"]))[2:].upper()

        validity = ca_certificate["validity"].native
        self.validity_start = str(validity["not_before"])
        self.validity_end = str(validity["not_after"])

        if entry.get_raw("certificateTemplates"):
            self.certificate_templates = list(
                map(lambda x: x.decode(), entry.get_raw("certificateTemplates"))
            )
        else:
            self.certificate_templates = []

        # EDITF_ATTRIBUTESUBJECTALTNAME2
        self.user_specifies_san = (edit_flags & 0x00040000) == 0x00040000

    def to_dict(self) -> dict:
        output = {"CA Name": self.ca_name,
                  "DNS Name": self.dns_name,
                  "Certificate Subject": self.subject_name,
                  "Certificate Serial Number": self.serial_number,
                  "Certificate Validity Start": self.validity_start,
                  "Certificate Validity End": self.validity_end,
                  "User Specified SAN": ("Enabled" if self.user_specifies_san else "Disabled")
                  }

        if self.security_descriptor is not None:
            # If security_descriptor is none, it is likely that it could not be
            # retrieved from remote registry

            ca_permissions = {}
            access_rights = {}

            ca_permissions["Owner"] = self.instance.translate_sid(
                self.security_descriptor.owner
            )

            for sid, rights in self.security_descriptor.aces.items():
                ca_rights = CERTIFICATION_AUTHORITY_RIGHTS(rights["rights"]).to_list()
                for ca_right in ca_rights:
                    if ca_right not in access_rights:
                        access_rights[ca_right] = [self.instance.translate_sid(sid)]
                    else:
                        access_rights[ca_right].append(self.instance.translate_sid(sid))

            ca_permissions["Access Rights"] = access_rights

            output["CA Permissions"] = ca_permissions

        return output


class CertificateTemplate:
    ATTRIBUTES = [
        "cn",
        "name",
        "pKIExpirationPeriod",
        "pKIOverlapPeriod",
        "msPKI-Certificate-Name-Flag",
        "msPKI-Enrollment-Flag",
        "msPKI-RA-Signature",
        "pKIExtendedKeyUsage",
        "nTSecurityDescriptor",
        "msPKI-Private-Key-Flag"
    ]

    def __init__(self, entry: LDAPEntry, instance: "Find", enrollment_services: List[EnrollmentService]):
        self._is_vulnerable = None
        self._can_enroll = None
        self._has_vulnerable_acl = None
        self._vulnerable_reasons = []
        self._enrollee = None
        self._vulnerable_technique_ids = []
        self.entry = entry
        self.instance = instance

        self.cas = [x for x in enrollment_services if entry.get("cn") in x.certificate_templates]

        # check to see if esc6 is possible
        self.esc6_vuln = any([x.user_specifies_san for x in self.cas])

        """
        if entry.get_raw("certificateTemplates"):
            self.certificate_templates = list(
                map(lambda x: x.decode(), entry.get_raw("certificateTemplates"))
            )
        else:
            self.certificate_templates = []

        """

        self.enabled = len(self.cas) > 0
        self.name = entry.get("name")

        self.validity_period = filetime_to_str(entry.get_raw("pKIExpirationPeriod")[0])
        self.renewal_period = filetime_to_str(entry.get_raw("pKIOverlapPeriod")[0])

        self.certificate_name_flag = MS_PKI_CERTIFICATE_NAME_FLAG(
            int(entry.get("msPKI-Certificate-Name-Flag"))
        )
        self.enrollment_flag = MS_PKI_ENROLLMENT_FLAG(
            int(entry.get("msPKI-Enrollment-Flag"))
        )

        self.private_key_flag = MS_PKI_PRIVATE_KEY_FLAG(
            int(entry.get("msPKI-Private-Key-Flag"))
        )

        self.authorized_signatures_required = int(entry.get("msPKI-RA-Signature"))

        # H3 addition - get policy attributes
        # TODO - add schema check
        app_policy_attributes = entry.get_raw("msPKI-RA-Application-Policies")
        # make sure it's a list
        if not isinstance(app_policy_attributes, list):
            if app_policy_attributes is None:
                app_policy_attributes = []
            else:
                app_policy_attributes = [app_policy_attributes]

        # less confusing than the lambda funcs of the original author
        app_policy_attributes = [x.decode() for x in app_policy_attributes]
        self.app_policy_attributes = [OID_TO_STR_MAP.get(x, x) for x in app_policy_attributes]

        eku = entry.get_raw("pKIExtendedKeyUsage")
        if not isinstance(eku, list):
            if eku is None:
                eku = []
            else:
                eku = [eku]

        eku = list(map(lambda x: x.decode(), eku))

        self.extended_key_usage = list(
            map(lambda x: OID_TO_STR_MAP[x] if x in OID_TO_STR_MAP else x, eku)
        )

        self.security_descriptor = ActiveDirectorySecurity(
            entry.get_raw("nTSecurityDescriptor")[0]
        )

    def __repr__(self) -> str:
        return "<CertificateTemplate name=%s>" % repr(self.name)

    @property
    def can_enroll(self) -> bool:
        if self._can_enroll is not None:
            return self._can_enroll

        user_can_enroll = False

        aces = self.security_descriptor.aces
        for sid, rights in aces.items():
            if not is_low_priv_sid(sid) and sid not in self.instance.user_sids:
                continue

            if (
                    EXTENDED_RIGHTS_NAME_MAP["All-Extended-Rights"]
                    in rights["extended_rights"]
                    or EXTENDED_RIGHTS_NAME_MAP["Enroll"]
                    in rights["extended_rights"]
                    or EXTENDED_RIGHTS_NAME_MAP["AutoEnroll"]
                    in rights["extended_rights"]
            ):
                self._enrollee = self.instance.translate_sid(sid)
                user_can_enroll = True

        self._can_enroll = user_can_enroll
        return self._can_enroll

    @property
    def has_vulnerable_acl(self) -> bool:
        if self._has_vulnerable_acl is not None:
            return self._has_vulnerable_acl

        vulnerable_acl = False
        aces = self.security_descriptor.aces
        vulnerable_acl_sids = []
        for sid, rights in aces.items():
            if not is_low_priv_sid(sid) and sid not in self.instance.user_sids:
                continue

            ad_rights = rights["rights"]
            if any(
                    right in ad_rights
                    for right in [
                        ACTIVE_DIRECTORY_RIGHTS.GENERIC_ALL,
                        ACTIVE_DIRECTORY_RIGHTS.WRITE_OWNER,
                        ACTIVE_DIRECTORY_RIGHTS.WRITE_DACL,
                        ACTIVE_DIRECTORY_RIGHTS.WRITE_PROPERTY,
                    ]
            ):
                vulnerable_acl_sids.append(repr(self.instance.translate_sid(sid)))
                vulnerable_acl = True
        if vulnerable_acl:
            self._vulnerable_reasons.append(
                "%s has dangerous permissions" % ' & '.join(vulnerable_acl_sids)
            )
        self._has_vulnerable_acl = vulnerable_acl
        return self._has_vulnerable_acl

    @property
    def has_authentication_eku(self) -> bool:
        return (
                any(
                    eku in self.extended_key_usage
                    for eku in [
                        "Client Authentication",
                        "Smart Card Logon",
                        "PKINIT Client Authentication",
                        "Any Purpose",
                    ]
                )
                or len(self.extended_key_usage) == 0
        )

    @property
    def requires_manager_approval(self) -> bool:
        return MS_PKI_ENROLLMENT_FLAG.PEND_ALL_REQUESTS in self.enrollment_flag

    @property
    def alt_requires_dns(self):
        return MS_PKI_CERTIFICATE_NAME_FLAG.SUBJECT_ALT_REQUIRE_DNS in self.certificate_name_flag

    @property
    def requires_private_key_archival(self):
        return MS_PKI_PRIVATE_KEY_FLAG.CT_FLAG_REQUIRE_PRIVATE_KEY_ARCHIVAL in self.private_key_flag

    # https://github.com/GhostPack/Certify/blob/2b1530309c0c5eaf41b2505dfd5a68c83403d031/Certify/Commands/Find.cs#L581
    @property
    def is_vulnerable(self) -> bool:
        """
        H3 - Rewrote for readability and added esc6 and esc3 checks.
        @return: True if vulnerable, False otherwise
        """
        if self._is_vulnerable is not None:
            return self._is_vulnerable

        self._is_vulnerable = False

        # ESC4 allows us to overwrite the manager approval, so check it first
        self.check_for_esc4()
        if self.requires_manager_approval:
            # manager approval negates remaining ESC paths
            return self._is_vulnerable

        if self.authorized_signatures_required > 0:
            return self.check_for_esc3_secondary_reqs()

        self.check_for_esc1_and_esc6()
        self.check_for_esc2()
        self.check_for_esc3_primary()
        return self._is_vulnerable

    @property
    def requires_cert_request_agent_policy(self) -> bool:
        return "Certificate Request Agent" in self.app_policy_attributes

    def vuln_reasons(self) -> str:
        output = ''
        for reason in self._vulnerable_reasons:
            output += f"{reason}\n"
        return output

    def vuln_ids(self) -> List['str']:
        return self._vulnerable_technique_ids

    def to_dict(self) -> dict:
        output = {"CAs": [ca.ca_name for ca in self.cas],
                  "Template Name": self.name,
                  "Validity Period": self.validity_period,
                  "Renewal Period": self.renewal_period,
                  "Certificate Name Flag": self.certificate_name_flag.to_str_list(),
                  "Enrollment Flag": self.enrollment_flag.to_str_list(),
                  "Authorized Signatures Required": self.authorized_signatures_required,
                  "Extended Key Usage": self.extended_key_usage,
                  "Private Key Flag": self.private_key_flag.to_str_list()
                  }

        permissions = {}

        enrollment_permissions = {}

        enrollment_rights = []
        all_extended_rights = []

        for sid, rights in self.security_descriptor.aces.items():
            if (
                    EXTENDED_RIGHTS_NAME_MAP["Enroll"]
                    in rights["extended_rights"]
                    or EXTENDED_RIGHTS_NAME_MAP["AutoEnroll"]
                    in rights["extended_rights"]
            ):
                enrollment_rights.append(self.instance.translate_sid(sid))
            if (
                    EXTENDED_RIGHTS_NAME_MAP["All-Extended-Rights"]
                    in rights["extended_rights"]
            ):
                all_extended_rights.append(self.instance.translate_sid(sid))

        if len(enrollment_rights) > 0:
            enrollment_permissions["Enrollment Rights"] = enrollment_rights

        if len(all_extended_rights) > 0:
            enrollment_permissions["All Extended Rights"] = all_extended_rights

        if len(enrollment_permissions) > 0:
            permissions["Enrollment Permissions"] = enrollment_permissions

        object_control_permissions = {"Owner": self.instance.translate_sid(
            self.security_descriptor.owner
        )}

        rights_mapping = [
            (ACTIVE_DIRECTORY_RIGHTS.GENERIC_ALL, [], "Full Control Principals"),
            (ACTIVE_DIRECTORY_RIGHTS.WRITE_OWNER, [], "Write Owner Principals"),
            (ACTIVE_DIRECTORY_RIGHTS.WRITE_DACL, [], "Write Dacl Principals"),
            (ACTIVE_DIRECTORY_RIGHTS.WRITE_PROPERTY, [], "Write Property Principals"),
        ]
        for sid, rights in self.security_descriptor.aces.items():
            rights = rights["rights"]
            sid = self.instance.translate_sid(sid)

            for (right, principal_list, _) in rights_mapping:
                if right in rights:
                    principal_list.append(sid)

        for _, rights, name in rights_mapping:
            if len(rights) > 0:
                object_control_permissions[name] = rights

        if len(object_control_permissions) > 0:
            permissions["Object Control Permissions"] = object_control_permissions

        if len(permissions) > 0:
            output["Permissions"] = permissions

        if len(self._vulnerable_reasons) > 0:
            output["Vulnerable Reasons"] = self._vulnerable_reasons
        if len(self._vulnerable_technique_ids) > 0:
            output["Vulnerable Technique IDs"] = self._vulnerable_technique_ids
        return output

    def check_for_esc4(self) -> None:
        """
        Checks if the template is vulnerable to ESC4
        @return:
        """

        owner_sid = self.security_descriptor.owner

        if owner_sid in self.instance.user_sids or is_low_priv_sid(owner_sid):
            self._vulnerable_reasons.append(
                "Template is owned by %s" % repr(self.instance.translate_sid(owner_sid))
            )
            self._vulnerable_technique_ids.append(ESC4)
            self._is_vulnerable = True

        vulnerable_acl = self.has_vulnerable_acl

        if vulnerable_acl:
            self._is_vulnerable = True
            self._vulnerable_technique_ids.append(ESC4)

    def check_for_esc3_secondary_reqs(self) -> bool:
        """
        Checks if the template meets requirements for an ESC3 secondary template
        @return: True if meets requirements
        """
        if (self.authorized_signatures_required == 1
                and self.can_enroll
                and self.has_authentication_eku
                and self.requires_cert_request_agent_policy):
            self._vulnerable_reasons.append(
                "%s can enroll, template has Certificate Request Agent EKU in Application Policy, allows Client "
                "Authentication, but requires authorized signature. This can only be abused if a "
                "ESC3 template has been identified." % repr(self._enrollee))
            self._vulnerable_technique_ids.append(ESC3_1)
            self._is_vulnerable = True
            return True
        return False

    def check_for_esc1_and_esc6(self) -> None:
        """
        Checks if the template is vulnerable to ESC1 or ESC6
        @return:
        """
        enrollee_supplies_subject = any(
            flag in self.certificate_name_flag
            for flag in [
                MS_PKI_CERTIFICATE_NAME_FLAG.ENROLLEE_SUPPLIES_SUBJECT,
                MS_PKI_CERTIFICATE_NAME_FLAG.ENROLLEE_SUPPLIES_SUBJECT_ALT_NAME,
            ]
        )

        if (
                self.can_enroll
                and (enrollee_supplies_subject or self.esc6_vuln)
                and self.has_authentication_eku
        ):
            alt_name_reason = ''
            if self.esc6_vuln:
                alt_name_reason += "CA allows GLOBAL specification of Subject Alternative Name (ESC6), "
                self._vulnerable_technique_ids.append(ESC6)
            if enrollee_supplies_subject:
                self._vulnerable_technique_ids.append(ESC1)
                alt_name_reason += "enrollee supplies subject, "

            self._vulnerable_reasons.append(f"{self._enrollee} can enroll,{alt_name_reason} and template allows "
                                            f"authentication")
            self._is_vulnerable = True

    def check_for_esc2(self) -> None:
        """
        Checks if the template is vulnerable to ESC2
        @return:
        """
        has_dangerous_eku = (
                any(
                    eku in self.extended_key_usage
                    for eku in ["Any Purpose"]
                )
                or len(self.extended_key_usage) == 0
        )

        if self.can_enroll and has_dangerous_eku:
            self._vulnerable_reasons.append(
                ("%s can enroll and template has dangerous EKU" % repr(self._enrollee))
            )
            self._vulnerable_technique_ids.append(ESC2)
            self._is_vulnerable = True

    def check_for_esc3_primary(self) -> None:
        """
        Checks if template meets requirements to be used as a primary ESC3 template
        @return:
        """

        if self.can_enroll and "Certificate Request Agent" in self.extended_key_usage:
            self._vulnerable_reasons.append(
                ("%s can enroll and template has Certificate Request Agent EKU set, this can only be abused if a "
                 "second template matches certain conditions" % repr(self._enrollee))
            )
            self._vulnerable_technique_ids.append(ESC3)
            self._is_vulnerable = True
